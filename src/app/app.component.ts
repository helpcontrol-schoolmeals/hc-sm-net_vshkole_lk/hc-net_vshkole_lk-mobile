import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public app$: Observable<any> = of({ version: '0.0.0' });
  public appPages = [
    {
      title: 'Личный кабинет',
      url: '/home',
      icon: 'home'
    },
    { title: 'Вход', url: '/signin', icon: 'signin' },
    {
      title: 'Профиль',
      url: '/list',
      icon: 'md-person'
    },
    {
      title: 'Услуги',
      url: '/list',
      icon: 'list'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
