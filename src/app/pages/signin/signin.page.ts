import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/services/auth.service';
import { first, map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import {
  RootStoreState,
  AuthStoreActions,
  AuthStoreSelectors
} from '../../root-store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss']
})
export class SigninPage implements OnInit {
  phone: string = '9832093436';
  password: string = 'qazwsx12345678Riga';
  error: string;

  isAuthenticated$: Observable<boolean>;
  hasError$: Observable<boolean>;
  erroMessage$: Observable<string>;
  constructor(
    private store$: Store<RootStoreState.State>,
    private auth: AuthService
  ) {}

  ngOnInit() {
    this.isAuthenticated$ = this.store$.pipe(
      select(AuthStoreSelectors.selectEnabledAuthenication)
    );
    this.erroMessage$ = this.store$.pipe(
      select(AuthStoreSelectors.selectError)
    );
    this.hasError$ = this.erroMessage$.pipe(map(val => !!val));
  }

  onLogin() {
    const action = new AuthStoreActions.SignInRequestAction({
      password: this.password,
      phone: this.phone
    });
    this.store$.dispatch(action);
    // this.auth
    //   .login({ password: this.password, phone: this.phone })
    //   .pipe(first())
    //   .subscribe(
    //     result => {}, // this.router.navigate(['todos']),
    //     err => (this.error = 'Ошибка авторизации')
    //   );
  }
}
