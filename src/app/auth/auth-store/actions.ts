import { Action } from '@ngrx/store';
import { ISignInCredentional, IAuthCredentional } from './models/interfacies';

export enum ActionTypes {
  SIGNIN_REQUEST = '[AUTH] SignIn Reguest',
  SIGNIN_FAILURE = '[AUTH] SignIn Failure',
  SIGNIN_SUCCESS = '[AUTH] SignIn Success'
}

export class SignInRequestAction implements Action {
  readonly type = ActionTypes.SIGNIN_REQUEST;
  constructor(public payload: ISignInCredentional) {}
}

export class SignInFailureAction implements Action {
  readonly type = ActionTypes.SIGNIN_FAILURE;
  constructor(public payload: { error: string }) {}
}

export class SignInSuccessAction implements Action {
  readonly type = ActionTypes.SIGNIN_SUCCESS;
  constructor(public payload: IAuthCredentional) {}
}

export type Actions =
  | SignInRequestAction
  | SignInFailureAction
  | SignInSuccessAction;
