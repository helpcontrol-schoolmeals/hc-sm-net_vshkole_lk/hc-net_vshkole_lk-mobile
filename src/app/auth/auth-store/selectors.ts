import {
  createFeatureSelector,
  createSelector,
  MemoizedSelector
} from '@ngrx/store';
import { State } from './state';
import { IAuthCredentional } from './models/interfacies';

// NOTE: методы получения свойст State
const getEnabledAuthentication = (state: State): boolean =>
  state.isAuthenticated;
const getAuthInProgress = (state: State): boolean => state.inProgress;
const getAuthCredentionals = (state: State) => state.credentionals;
const getAuthErrorMessage = (state: State) => state.errorMessage;

// NOTE: селекторы для получения доступа к данным из сосотяния

export const selectAuthStoreState: MemoizedSelector<
  object,
  State
> = createFeatureSelector<State>('AuthStore');

export const selectEnabledAuthenication: MemoizedSelector<
  object,
  boolean
> = createSelector(
  selectAuthStoreState,
  getEnabledAuthentication
);

export const selectError: MemoizedSelector<object, string> = createSelector(
  selectAuthStoreState,
  getAuthErrorMessage
);

export const selectCredentionals: MemoizedSelector<
  object,
  IAuthCredentional
> = createSelector(
  selectAuthStoreState,
  getAuthCredentionals
);

export const selectAuthEnabledProgress: MemoizedSelector<
  object,
  boolean
> = createSelector(
  selectAuthStoreState,
  getAuthInProgress
);
