import { initialState, State } from './state';
import { Actions, ActionTypes } from './actions';
import { IAuthCredentional, ISignInCredentional } from './models/interfacies';

export function authReducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.SIGNIN_REQUEST:
      return { ...state, inProgress: true, errorMessage: null };

    case ActionTypes.SIGNIN_SUCCESS:
      // const credentionals = action.payload;
      const credentionals: IAuthCredentional = action.payload;
      return {
        ...state,
        inProgress: false,
        errorMessage: null,
        isAuthenticated: true,
        credentionals
      };

    case ActionTypes.SIGNIN_FAILURE: {
      const credentionals_: IAuthCredentional = state.credentionals;
      if (!!credentionals_) {
        credentionals_.token = null;
      }
      const errorMessage = action.payload.error;
      return {
        ...state,
        isAuthenticated: false,
        inProgress: false,
        errorMessage,
        credentionals: credentionals_
      };
    }
    default:
      return state;
  }
  return;
}
