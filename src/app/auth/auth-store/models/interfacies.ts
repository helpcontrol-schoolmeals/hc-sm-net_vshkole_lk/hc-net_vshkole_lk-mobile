/**
 * @description Данные для выполнения авторизации на сервере
 *
 * @property {string} phone Номер телефона
 * @property {string} password Персональный пароль
 * @export
 * @interface ISignInCredentional
 */
export interface ISignInCredentional {
  phone: string;
  password: string;
}

export interface IAuthCredentional {
  phone: string;
  token: string;
}
