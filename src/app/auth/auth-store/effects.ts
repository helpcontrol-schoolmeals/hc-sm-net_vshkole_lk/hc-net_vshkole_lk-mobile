import { Injectable } from '@angular/core';
import { Observable, of as observableOf } from 'rxjs';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AuthService } from './../services/auth.service';
import * as authActions from './actions';
import { switchMap, map, catchError } from 'rxjs/operators';

@Injectable()
export class AuthStoreEffects {
  @Effect()
  signInRequestEffect$: Observable<Action> = this.actions$.pipe(
    ofType<authActions.SignInRequestAction>(
      authActions.ActionTypes.SIGNIN_REQUEST
    ),
    switchMap(action =>
      this.authService.login(action.payload).pipe(
        map(
          (result: { token: string }) =>
            new authActions.SignInSuccessAction({
              phone: action.payload.phone,
              token: result.token
            })
        ),
        catchError(error =>
          observableOf(
            new authActions.SignInFailureAction({ error: error.message })
          )
        )
      )
    )
  );
  constructor(private actions$: Actions, private authService: AuthService) {}
}
