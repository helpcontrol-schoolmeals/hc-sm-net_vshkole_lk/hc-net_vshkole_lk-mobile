import { ISignInCredentional, IAuthCredentional } from './models/interfacies';

export interface State {
  isAuthenticated: boolean;
  credentionals: IAuthCredentional | null;
  errorMessage: string | null;
  inProgress: boolean;
}

export const initialState = {
  isAuthenticated: false,
  credentionals: null,
  errorMessage: null,
  inProgress: false
};
