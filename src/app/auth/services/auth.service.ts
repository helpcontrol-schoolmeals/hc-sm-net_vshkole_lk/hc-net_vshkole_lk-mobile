import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { IAuthCredentional } from '../auth-store/models/interfacies';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private jwtHelper: JwtHelperService = new JwtHelperService();

  constructor(private http: HttpClient) {}

  login(credentionals: any): Observable<IAuthCredentional> {
    // credentionals = { phone: '9832093436', password: 'qazwsx12345678Riga' };
    return this.http
      .post<{ token: string }>(
        'https://lk.vshkole.net/api/auth/signin',
        credentionals
      )
      .pipe(
        tap(console.log),
        map(result => {
          localStorage.setItem('sc-user_token', result.token);
          return result;
        })
      );
  }
}
