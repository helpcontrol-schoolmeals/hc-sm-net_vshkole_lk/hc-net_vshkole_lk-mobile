import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AuthStoreEffects } from './auth-store/effects';
import { authReducer } from './auth-store/reducers';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('AuthStore', authReducer),
    EffectsModule.forFeature([AuthStoreEffects])
  ],
  providers: [AuthStoreEffects]
})
export class AuthModule {}
