import { AuthStoreSate } from '../auth';

// NOTE: место для добавления import Feature Store Module

export interface State {
  version: string;
  // NOTE: меcто добавления states Feature Store Module (MyReatureStoreState.State)
  auth: AuthStoreSate.State;
}
