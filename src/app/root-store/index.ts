import { RootStoreModule } from './root-store.module';
import * as RootStoreSelectors from './selectors';
import * as RootStoreState from './state';
// NOTE: место добавления export feature-store
// DEMO: export * from './my-feature-store';
export * from './../auth';
export { RootStoreState, RootStoreSelectors, RootStoreModule };
